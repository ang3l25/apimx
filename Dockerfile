# Imagen base
FROM node:latest

# Diretorio de la app en el contenedor
WORKDIR /app

# Copiado de archivos
ADD . /app

# Dependencias
RUN npm install

# Puerto que expongo
EXPOSE 3000

# Comando para ejecutar servicio
CMD ["npm", "start"]
