//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/anegrete/collections"
var urlAPIRaiz = "https://www.banxico.org.mx/SieAPIRest/service/v1/series/"

var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var token = "a399943b194dc89fa61be5c76d9c1c6abb9f47df4675bb5a21b2cc27e58b409f";

var clienteMlabRaiz;
var clienteAPI;

var urlClientes = "https://api.mlab.com/api/1/databases/anegrete/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLab = requestjson.createClient(urlClientes)

var urlUsuarios = "https://api.mlab.com/api/1/databases/anegrete/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuarioMLab = requestjson.createClient(urlUsuarios)

var urlCuentas = "https://api.mlab.com/api/1/databases/anegrete/collections/Cuentas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var cuentaMLab = requestjson.createClient(urlCuentas)


var urlMovimientos = "https://api.mlab.com/api/1/databases/anegrete/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var movimientoMLab = requestjson.createClient(urlMovimientos)

app.listen(port);

var bodyparser = require('body-parser');
app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Request-With, Content-Type, Accept")
  next()
})

var movimientosJSON = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

/**
 * API BANXICO
 * Serie histórica del tipo de cambio Tipo de cambio peso dólar desde 2020
 */
app.get('/v2/Dolares', function(req, res){
  var serie = "SF63528";
  var fechaI = "2020-01-01";
  var fechaF = "2020-12-31";
  var filtro = `/${serie}/datos/${fechaI}/${fechaF}?token=`
  var urlAPI = urlAPIRaiz + filtro + token;

  clienteAPI= requestjson.createClient(urlAPI);
  clienteAPI.get("",function (err, resM, body) {
    if (!err) {
      res.send(body.bmx.series[0].datos);
    } else {
      res.status(404).send('Error en el servicio');
    }
  });


})

/**
 * Gestion de Clientes
 */

app.get('/Clientes', function(req, res){
  clienteMLab.get('', function (err, resM, body) {
    if(err){
      console.log(body)
    }else{
      res.send(body);
    }
  })
})

app.post('/Clientes', function(req, res) {
  clienteMLab.post('', req.body, function (err, resM, body) {
    res.send(body)
  })
})
/**
 * Gestiona Cuentas
 */
app.post('/Cuenta', function(req, res) {
  var idCliente = req.body.id
  var tarjeta = req.body.tarjeta.replace(/[' ']*/g,'')
  var credito = req.body.credito
  
  var json = {
    "idCliente":idCliente,
    "tarjeta":tarjeta,
    "lineaCredito":credito,
    "saldoUtilizado":0,
    "saldoDisponible":new Number(credito)
  }
  cuentaMLab.post('', json, function (err, resM, body) {
    if (!err) {
      res.status(200).send('Transaccion correcta');
    } else {
      res.status(404).send('Error en el servicio');
    }
    
  })
})

app.post('/v2/Cuenta', function(req, res) {
  var id = req.body.id;
  var tarjeta = req.body.tarjeta;  
  var query=`q={
    "idCliente":"${id}",
    "tarjeta":"${tarjeta}"
  }`
  var body={
    "$set":{
      "saldoUtilizado":req.body.saldoUtilizado,
      "saldoDisponible":req.body.saldoDisponible
    }
  }
  var urlMLab = urlMlabRaiz + "/Cuentas?" + query + "&" + apiKey;

  clienteMlabRaiz = requestjson.createClient(urlMLab);
  clienteMlabRaiz.put("",body ,function (err, resM, body) {
    if (!err) {
      res.status(200).send('Transaccion correcta');
    } else {
      res.status(404).send('Error en el servicio');
    }
  });

})


/**
 * Obtencion de tarjetas 
 */
app.get("/v2/Tarjetas/:idCliente", function (req, res) {
  var query = 'q={"idCliente":"' + req.params.idCliente + '"}';
  var urlMLab = urlMlabRaiz + "/Cuentas?" + query + "&" + apiKey;

  clienteMlabRaiz = requestjson.createClient(urlMLab);
  clienteMlabRaiz.get("", function (err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(getCard(body));
    }
  });
});

app.get("/v2/Tarjetas/:idClient/:idCard", function (req, res) {
  var query=`q={
    "idCliente":"${req.params.idClient}",
    "tarjeta":"${req.params.idCard}"
  }`
  var urlMLab = urlMlabRaiz + "/Cuentas?" + query + "&" + apiKey;

  clienteMlabRaiz = requestjson.createClient(urlMLab);
  clienteMlabRaiz.get("", function (err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);      
    }
  });
});

function getCard(arrayIn) {   
  var arrayJson = [];
  
  for (var a = 0; a < arrayIn.length; a++) {
    var json ='{"cuenta":"' + arrayIn[a].tarjeta + '"}'
    arrayJson.push(JSON.parse(json));
  }

  return arrayJson;  
}


/**
 * Gestion de Movimientos
 */

app.post('/Movimiento', function(req, res) {
  movimientoMLab.post('', req.body, function (err, resM, body) {
    if (!err) {
      res.status(200).send('Transaccion correcta');
    } else {
      res.status(404).send('Error en el servicio');
    }
  })
})

app.get('/v1/Movimientos', function (req, res) {
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos/:idCliente', function (req, res) {
  var query = 'q={"idCliente":"' + req.params.idCliente + '"}';
  var urlMLab = urlMlabRaiz + "/Movimientos?" + query + "&" + apiKey;

  clienteMlabRaiz = requestjson.createClient(urlMLab);
  clienteMlabRaiz.get("", function (err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  });
})

app.get('/v2/Movimientos/:idCliente/:idCuenta', function (req, res) {
  var tarjeta = req.params.idCuenta.replace(/[' ']*/g,'')
  var query = `q={"idCliente": "${req.params.idCliente}", "tarjeta":"${tarjeta}"}`;
  var urlMLab = urlMlabRaiz + "/Movimientos?" + query + "&" + apiKey;

  clienteMlabRaiz = requestjson.createClient(urlMLab);
  clienteMlabRaiz.get("", function (err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  });
})

app.get('/v2/Movimientos/:idCliente/:idCuenta/:min/:max', function (req, res) {
  var cliente = req.params.idCliente;
  var cuenta = req.params.idCuenta;  
  var min = req.params.min;
  var max = req.params.max;
  var query = `q={
    "idCliente":"${cliente}",
    "tarjeta":"${cuenta}",
    "importe":{
      "$gte":${min},
      "$lte":${max}
    }
  }`
  var urlMLab = urlMlabRaiz + "/Movimientos?" + query + "&" + apiKey;

  clienteMlabRaiz = requestjson.createClient(urlMLab);
  clienteMlabRaiz.get("", function (err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  });
})

app.get('/v2/MovimientosQuery', function (req, res) {
  console.log(req.query);
  res.send("Se recibio el query");
})

app.post('/v2/Movimientos', function (req, res) {
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
})



/**
 * Gestiona el inicio de sesión & registro de nuevos usuarios
 */
app.post('/Sigin', function(req, res) {
  usuarioMLab.post('', req.body, function (err, resM, body) {
    res.send(body)
  })
})

app.post('/Login', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password
  var query = 'q={"email":"'+ email +'","password":"' +password + '"}'
  var urlMLab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;  
  clienteMlabRaiz = requestjson.createClient(urlMLab)
  clienteMlabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length == 1) { //Login ok, se encontro 1 documento
        var obj = {
          "id": body[0]._id.$oid,
          "nombre": body[0].nombre
        }        
        res.status(200).send(obj)
      } else { //No se encontro al usuario
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

app.get("/v2/Correos", function (req, res) {
  
  var urlMLab = urlMlabRaiz + "/Usuarios?" + apiKey;

  clienteMlabRaiz = requestjson.createClient(urlMLab);
  clienteMlabRaiz.get("", function (err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(getEmail(body));
    }
  });
});

function getEmail(arrayIn) {   
  var arrayJson = [];
  
  for (var a = 0; a < arrayIn.length; a++) {
    var json ='{"email":"' + arrayIn[a].email + '"}'
    arrayJson.push(JSON.parse(json));
  }

  return arrayJson;  
}
